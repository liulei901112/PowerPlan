# Windows电源计划自动切换

#### 项目介绍
基于Windows电源计划，实现自动切换电源计划。解决高功率台式机在离开后自动切换为节能，有操作切换为平衡。有效降低离开后资源浪费的问题

#### 软件架构
基于.net framework 4.0，使用winform窗口开发。通过执行cmd命令，获取电源计划列表、设置电源计划。主界面另起线程，代替Timer执行时间检查和空闲检查。


#### 安装教程

编译后直接运行exe

#### 使用说明
1. Windows控制面板 - 电源选项 - 创建电源计划，基于【节能】创建【夜间】和【离开】模式电源计划
![Windows电源选项](https://images.gitee.com/uploads/images/2020/0505/005837_bcb22241_995027.png "屏幕截图.png")
![Windows创建电源计划](https://images.gitee.com/uploads/images/2020/0505/005925_f96c064a_995027.png "屏幕截图.png")
2. 运行编译后的exe
![双击电源计划列表电源计划，切换当前电源计划和默认电源计划](https://images.gitee.com/uploads/images/2020/0505/010243_4544e2c3_995027.png "屏幕截图.png")
3. 主界面说明
![空闲切换时间，默认30s](https://images.gitee.com/uploads/images/2020/0505/010442_60d35dbe_995027.png "屏幕截图.png")
![默认电源计划，默认平衡。从离开模式或夜间模式切换回默认模式时使用](https://images.gitee.com/uploads/images/2020/0505/010515_5bc1fe18_995027.png "屏幕截图.png")
![当启用离开模式时，离开模式所使用的电源计划可设置，默认节能](https://images.gitee.com/uploads/images/2020/0505/010609_ea35cdc2_995027.png "屏幕截图.png")
![当启用夜间模式时，夜间模式所使用的电源计划可设置，默认节能](https://images.gitee.com/uploads/images/2020/0505/010653_17b76e35_995027.png "屏幕截图.png")
![夜间模式的夜间开始和结束时间](https://images.gitee.com/uploads/images/2020/0505/010736_88b92c6c_995027.png "屏幕截图.png")
![键盘鼠标无操作30s后切换为离开模式，有操作后自动切换为默认模式：平衡](https://images.gitee.com/uploads/images/2020/0505/010916_ceaa8840_995027.png "屏幕截图.png")
4. 主窗口关闭按钮是最小化到托盘图标。如果想退出程序，找到托盘图标【电源计划】，右键选择退出即可

#### 参与贡献
1. Powercfg 命令行选项 https://docs.microsoft.com/zh-cn/windows-hardware/design/device-experiences/powercfg-command-line-options