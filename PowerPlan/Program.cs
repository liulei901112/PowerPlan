﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace PowerPlan
{
    static class Program
    {
        public static EventWaitHandle Started;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            // 尝试创建一个命名时间
            bool createNew;
            Started = new EventWaitHandle(false, EventResetMode.AutoReset, "PowerPlanStartEvent", out createNew);

            // 如果该命名时间已经存在（存在有前一个运行示例），则发时间通知并退出
            if (!createNew)
            {
                Started.Set();
                return;
            }

            // 启动实例
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
