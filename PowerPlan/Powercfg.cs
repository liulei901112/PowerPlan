﻿namespace PowerPlan
{
    public class Powercfg
    {
        public string UUID { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
