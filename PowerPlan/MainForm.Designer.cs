﻿namespace PowerPlan
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PowercfgList = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.logBox = new System.Windows.Forms.TextBox();
            this.LogMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.清空ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CPUUsageValue = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CPUUsage = new System.Windows.Forms.Label();
            this.FreeTimeShow = new System.Windows.Forms.Label();
            this.DefaultModelValue = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IdleTimeoutValue = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SpeedupModeValue = new System.Windows.Forms.ComboBox();
            this.LeaveModeValue = new System.Windows.Forms.ComboBox();
            this.SpeedupMode = new System.Windows.Forms.CheckBox();
            this.LeaveMode = new System.Windows.Forms.CheckBox();
            this.PowerPlanNotify = new System.Windows.Forms.NotifyIcon(this.components);
            this.PowerPlanNotifyMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.启动最小化ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ShutdownPresupposeTime = new System.Windows.Forms.Label();
            this.ButtonShutdown = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ShutdownRemainingTime = new System.Windows.Forms.Label();
            this.ShutdownSecondValue = new System.Windows.Forms.ComboBox();
            this.ShutdownMinuteValue = new System.Windows.Forms.ComboBox();
            this.ShutdownHourValue = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TimingValue = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.LogMenu.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.PowerPlanNotifyMenu.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PowercfgList);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 233);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "电源计划列表";
            // 
            // PowercfgList
            // 
            this.PowercfgList.Font = new System.Drawing.Font("宋体", 9F);
            this.PowercfgList.FormattingEnabled = true;
            this.PowercfgList.ItemHeight = 12;
            this.PowercfgList.Location = new System.Drawing.Point(6, 20);
            this.PowercfgList.Name = "PowercfgList";
            this.PowercfgList.Size = new System.Drawing.Size(165, 196);
            this.PowercfgList.TabIndex = 0;
            this.PowercfgList.DoubleClick += new System.EventHandler(this.PowercfgList_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.logBox);
            this.groupBox2.Location = new System.Drawing.Point(13, 252);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(662, 186);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "日志";
            // 
            // logBox
            // 
            this.logBox.ContextMenuStrip = this.LogMenu;
            this.logBox.Location = new System.Drawing.Point(7, 21);
            this.logBox.Multiline = true;
            this.logBox.Name = "logBox";
            this.logBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logBox.Size = new System.Drawing.Size(645, 156);
            this.logBox.TabIndex = 0;
            // 
            // LogMenu
            // 
            this.LogMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空ToolStripMenuItem});
            this.LogMenu.Name = "LogMenu";
            this.LogMenu.Size = new System.Drawing.Size(101, 26);
            // 
            // 清空ToolStripMenuItem
            // 
            this.清空ToolStripMenuItem.Name = "清空ToolStripMenuItem";
            this.清空ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.清空ToolStripMenuItem.Text = "清空";
            this.清空ToolStripMenuItem.Click += new System.EventHandler(this.清空ToolStripMenuItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CPUUsageValue);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.CPUUsage);
            this.groupBox3.Controls.Add(this.FreeTimeShow);
            this.groupBox3.Controls.Add(this.DefaultModelValue);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.IdleTimeoutValue);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.SpeedupModeValue);
            this.groupBox3.Controls.Add(this.LeaveModeValue);
            this.groupBox3.Controls.Add(this.SpeedupMode);
            this.groupBox3.Controls.Add(this.LeaveMode);
            this.groupBox3.Location = new System.Drawing.Point(197, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(259, 233);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "电源计划";
            // 
            // CPUUsageValue
            // 
            this.CPUUsageValue.FormattingEnabled = true;
            this.CPUUsageValue.Location = new System.Drawing.Point(108, 143);
            this.CPUUsageValue.Name = "CPUUsageValue";
            this.CPUUsageValue.Size = new System.Drawing.Size(121, 20);
            this.CPUUsageValue.TabIndex = 30;
            this.CPUUsageValue.SelectedIndexChanged += new System.EventHandler(this.CPUUsageValue_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 29;
            this.label4.Text = "CPU使用率大于";
            // 
            // CPUUsage
            // 
            this.CPUUsage.AutoSize = true;
            this.CPUUsage.ForeColor = System.Drawing.Color.Red;
            this.CPUUsage.Location = new System.Drawing.Point(25, 173);
            this.CPUUsage.Name = "CPUUsage";
            this.CPUUsage.Size = new System.Drawing.Size(89, 12);
            this.CPUUsage.TabIndex = 28;
            this.CPUUsage.Text = "CPU使用率：65%";
            // 
            // FreeTimeShow
            // 
            this.FreeTimeShow.AutoSize = true;
            this.FreeTimeShow.ForeColor = System.Drawing.Color.Red;
            this.FreeTimeShow.Location = new System.Drawing.Point(25, 85);
            this.FreeTimeShow.Name = "FreeTimeShow";
            this.FreeTimeShow.Size = new System.Drawing.Size(83, 12);
            this.FreeTimeShow.TabIndex = 27;
            this.FreeTimeShow.Text = "键鼠空闲：0秒";
            // 
            // DefaultModelValue
            // 
            this.DefaultModelValue.FormattingEnabled = true;
            this.DefaultModelValue.Location = new System.Drawing.Point(108, 18);
            this.DefaultModelValue.Name = "DefaultModelValue";
            this.DefaultModelValue.Size = new System.Drawing.Size(121, 20);
            this.DefaultModelValue.TabIndex = 14;
            this.DefaultModelValue.SelectedIndexChanged += new System.EventHandler(this.DefaultModelValue_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "默认电源计划";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(235, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "秒";
            // 
            // IdleTimeoutValue
            // 
            this.IdleTimeoutValue.FormattingEnabled = true;
            this.IdleTimeoutValue.Location = new System.Drawing.Point(108, 52);
            this.IdleTimeoutValue.Name = "IdleTimeoutValue";
            this.IdleTimeoutValue.Size = new System.Drawing.Size(121, 20);
            this.IdleTimeoutValue.TabIndex = 5;
            this.IdleTimeoutValue.SelectedIndexChanged += new System.EventHandler(this.IdleTimeoutValue_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "空闲切换时间";
            // 
            // SpeedupModeValue
            // 
            this.SpeedupModeValue.FormattingEnabled = true;
            this.SpeedupModeValue.Location = new System.Drawing.Point(108, 197);
            this.SpeedupModeValue.Name = "SpeedupModeValue";
            this.SpeedupModeValue.Size = new System.Drawing.Size(121, 20);
            this.SpeedupModeValue.TabIndex = 3;
            this.SpeedupModeValue.SelectedIndexChanged += new System.EventHandler(this.SpeedupModeValue_SelectedIndexChanged);
            // 
            // LeaveModeValue
            // 
            this.LeaveModeValue.FormattingEnabled = true;
            this.LeaveModeValue.Location = new System.Drawing.Point(108, 110);
            this.LeaveModeValue.Name = "LeaveModeValue";
            this.LeaveModeValue.Size = new System.Drawing.Size(121, 20);
            this.LeaveModeValue.TabIndex = 2;
            this.LeaveModeValue.SelectedIndexChanged += new System.EventHandler(this.LeaveModeValue_SelectedIndexChanged);
            // 
            // SpeedupMode
            // 
            this.SpeedupMode.AutoSize = true;
            this.SpeedupMode.Location = new System.Drawing.Point(9, 199);
            this.SpeedupMode.Name = "SpeedupMode";
            this.SpeedupMode.Size = new System.Drawing.Size(96, 16);
            this.SpeedupMode.TabIndex = 1;
            this.SpeedupMode.Text = "启动加速模式";
            this.SpeedupMode.UseVisualStyleBackColor = true;
            this.SpeedupMode.CheckedChanged += new System.EventHandler(this.SpeedupMode_CheckedChanged);
            // 
            // LeaveMode
            // 
            this.LeaveMode.AutoSize = true;
            this.LeaveMode.Location = new System.Drawing.Point(9, 112);
            this.LeaveMode.Name = "LeaveMode";
            this.LeaveMode.Size = new System.Drawing.Size(96, 16);
            this.LeaveMode.TabIndex = 0;
            this.LeaveMode.Text = "启用离开模式";
            this.LeaveMode.UseVisualStyleBackColor = true;
            this.LeaveMode.CheckedChanged += new System.EventHandler(this.LeaveMode_CheckedChanged);
            // 
            // PowerPlanNotify
            // 
            this.PowerPlanNotify.ContextMenuStrip = this.PowerPlanNotifyMenu;
            this.PowerPlanNotify.Icon = ((System.Drawing.Icon)(resources.GetObject("PowerPlanNotify.Icon")));
            this.PowerPlanNotify.Text = "电源计划";
            this.PowerPlanNotify.Visible = true;
            this.PowerPlanNotify.DoubleClick += new System.EventHandler(this.PowerPlanNotify_DoubleClick);
            // 
            // PowerPlanNotifyMenu
            // 
            this.PowerPlanNotifyMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.启动最小化ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.PowerPlanNotifyMenu.Name = "PowerPlanNotifyMenu";
            this.PowerPlanNotifyMenu.Size = new System.Drawing.Size(137, 48);
            // 
            // 启动最小化ToolStripMenuItem
            // 
            this.启动最小化ToolStripMenuItem.Name = "启动最小化ToolStripMenuItem";
            this.启动最小化ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.启动最小化ToolStripMenuItem.Text = "启动最小化";
            this.启动最小化ToolStripMenuItem.Click += new System.EventHandler(this.启动最小化ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ShutdownPresupposeTime);
            this.groupBox4.Controls.Add(this.ButtonShutdown);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.ShutdownRemainingTime);
            this.groupBox4.Controls.Add(this.ShutdownSecondValue);
            this.groupBox4.Controls.Add(this.ShutdownMinuteValue);
            this.groupBox4.Controls.Add(this.ShutdownHourValue);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.TimingValue);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(462, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(213, 233);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "关机计划";
            // 
            // ShutdownPresupposeTime
            // 
            this.ShutdownPresupposeTime.AutoSize = true;
            this.ShutdownPresupposeTime.ForeColor = System.Drawing.Color.Blue;
            this.ShutdownPresupposeTime.Location = new System.Drawing.Point(6, 143);
            this.ShutdownPresupposeTime.Name = "ShutdownPresupposeTime";
            this.ShutdownPresupposeTime.Size = new System.Drawing.Size(137, 12);
            this.ShutdownPresupposeTime.TabIndex = 26;
            this.ShutdownPresupposeTime.Text = "定时关机：00时00分12秒";
            // 
            // ButtonShutdown
            // 
            this.ButtonShutdown.Location = new System.Drawing.Point(68, 199);
            this.ButtonShutdown.Name = "ButtonShutdown";
            this.ButtonShutdown.Size = new System.Drawing.Size(75, 23);
            this.ButtonShutdown.TabIndex = 25;
            this.ButtonShutdown.Text = "开始";
            this.ButtonShutdown.UseVisualStyleBackColor = true;
            this.ButtonShutdown.Click += new System.EventHandler(this.ButtonShutdown_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(192, 113);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 24;
            this.label12.Text = "秒";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(192, 87);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 23;
            this.label11.Text = "分";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(192, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 22;
            this.label9.Text = "时";
            // 
            // ShutdownRemainingTime
            // 
            this.ShutdownRemainingTime.AutoSize = true;
            this.ShutdownRemainingTime.ForeColor = System.Drawing.Color.Red;
            this.ShutdownRemainingTime.Location = new System.Drawing.Point(6, 172);
            this.ShutdownRemainingTime.Name = "ShutdownRemainingTime";
            this.ShutdownRemainingTime.Size = new System.Drawing.Size(197, 12);
            this.ShutdownRemainingTime.TabIndex = 21;
            this.ShutdownRemainingTime.Text = "剩余时间：00秒（点击开始后更新）";
            // 
            // ShutdownSecondValue
            // 
            this.ShutdownSecondValue.FormattingEnabled = true;
            this.ShutdownSecondValue.Location = new System.Drawing.Point(65, 110);
            this.ShutdownSecondValue.Name = "ShutdownSecondValue";
            this.ShutdownSecondValue.Size = new System.Drawing.Size(121, 20);
            this.ShutdownSecondValue.TabIndex = 19;
            this.ShutdownSecondValue.SelectedIndexChanged += new System.EventHandler(this.ShutdownSecondValue_SelectedIndexChanged);
            // 
            // ShutdownMinuteValue
            // 
            this.ShutdownMinuteValue.FormattingEnabled = true;
            this.ShutdownMinuteValue.Location = new System.Drawing.Point(65, 84);
            this.ShutdownMinuteValue.Name = "ShutdownMinuteValue";
            this.ShutdownMinuteValue.Size = new System.Drawing.Size(121, 20);
            this.ShutdownMinuteValue.TabIndex = 18;
            this.ShutdownMinuteValue.SelectedIndexChanged += new System.EventHandler(this.ShutdownMinuteValue_SelectedIndexChanged);
            // 
            // ShutdownHourValue
            // 
            this.ShutdownHourValue.FormattingEnabled = true;
            this.ShutdownHourValue.Location = new System.Drawing.Point(65, 58);
            this.ShutdownHourValue.Name = "ShutdownHourValue";
            this.ShutdownHourValue.Size = new System.Drawing.Size(121, 20);
            this.ShutdownHourValue.TabIndex = 17;
            this.ShutdownHourValue.SelectedIndexChanged += new System.EventHandler(this.ShutdownHourValue_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 16;
            this.label6.Text = "时间设定";
            // 
            // TimingValue
            // 
            this.TimingValue.FormattingEnabled = true;
            this.TimingValue.Location = new System.Drawing.Point(65, 18);
            this.TimingValue.Name = "TimingValue";
            this.TimingValue.Size = new System.Drawing.Size(121, 20);
            this.TimingValue.TabIndex = 15;
            this.TimingValue.SelectedIndexChanged += new System.EventHandler(this.TimingValue_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 4;
            this.label10.Text = "计时方式";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 449);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "电源计划";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.LogMenu.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.PowerPlanNotifyMenu.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox logBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox LeaveMode;
        private System.Windows.Forms.CheckBox SpeedupMode;
        private System.Windows.Forms.ComboBox SpeedupModeValue;
        private System.Windows.Forms.ComboBox LeaveModeValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox IdleTimeoutValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox DefaultModelValue;
        private System.Windows.Forms.NotifyIcon PowerPlanNotify;
        private System.Windows.Forms.ContextMenuStrip PowerPlanNotifyMenu;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip LogMenu;
        private System.Windows.Forms.ToolStripMenuItem 清空ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox TimingValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox ShutdownHourValue;
        private System.Windows.Forms.ComboBox ShutdownSecondValue;
        private System.Windows.Forms.ComboBox ShutdownMinuteValue;
        private System.Windows.Forms.Label ShutdownRemainingTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ButtonShutdown;
        private System.Windows.Forms.Label ShutdownPresupposeTime;
        private System.Windows.Forms.ListBox PowercfgList;
        private System.Windows.Forms.Label FreeTimeShow;
        private System.Windows.Forms.ToolStripMenuItem 启动最小化ToolStripMenuItem;
        private System.Windows.Forms.Label CPUUsage;
        private System.Windows.Forms.ComboBox CPUUsageValue;
        private System.Windows.Forms.Label label4;
    }
}

